# mldong-doc-cli
基于VuePress2.x的文档脚手架，增加了express后端服务进行简单的权限控制

- [演示地址](https://doc-cli.mldong.com/)

演示环境密钥：123456

## 克隆项目

```bash
git clone https://gitee.com/mldong666/mldong-doc-cli.git
```
## 安装依赖

```bash
yarn install
# or
npm install
```
## 运行
```bash
yarn dev
# or
npm run dev
```
访问：http://localhost:8080/
## 打包
```bash
yarn build
# or
npm run build
```
## 服务端运行
```bash
yarn build && yarn server
# or
npm run build && npm run server
```
访问：http://localhost:3000/

## 生产部署
默认已经安装有docker和docker-compose，直接执行以下命令即可
### 构造镜像
```bash
docker build -t mldong-doc-cli .
```
### 启动容器
```bash
docker-compose up -d
```
### 查看服务状态 
```bash
docker-compose ps  
```
### 查看服务日志  
```bash
docker-compose logs mldong-doc-cli  
```
### 停止服务  
```bash
docker-compose stop mldong-doc-cli
```
### 重新生成镜像
```bash
docker-compose build --no-cache mldong-doc-cli
```