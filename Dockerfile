# 使用官方的Node.js 16作为基础镜像  
FROM node:16.16.0 
  
# 设置工作目录为/app  
WORKDIR /app  
  
# 将当前目录内容复制到容器的/app内  
COPY . /app  
  
# 设置淘宝npm镜像源（可选）  
RUN npm config set registry https://registry.npmmirror.com  
# 安装全局的PM2  
RUN npm install -g pm2  
  
# 使用npm来安装依赖  
RUN npm install  
  
# 暴露端口  
EXPOSE 3000  

# 使用pm2-runtime启动  
CMD ["pm2-runtime", "npm", "--", "run", "server"]
