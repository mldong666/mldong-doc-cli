---
needAuth: false
home: true
heroImage: /logo.png
actions:
  - text: 快速开始 →
    link: /quick-start.html
    type: primary
features:
  - title: 专注写作
    details: VuePress以Markdown为中心，让你专注于内容创作，无需繁琐配置。
  - title: Vue驱动
    details: 利用Vue的开发体验，Markdown与Vue组件完美结合，自由定制主题。
  - title: 高效引流
    details: 学习VuePress与Express结合，实战动态权限控制，提升开源项目引流效果。
---