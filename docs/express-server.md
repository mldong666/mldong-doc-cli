---
needAuth: true
---
# Express.js 应用服务器文档

## 一、环境变量配置

### 基础配置
- `PORT`: 应用运行的端口号，默认为`3000`。
- `DEFAULT_TOKENS`: 应用接受的默认认证令牌列表，以逗号分隔，默认值为`123456`。
- `COOKIE_MAX_AGE`: Cookie的最大生命周期（天），默认为`7`。
- `JWT_EXPIRES_IN`: JWT令牌的过期时间（天），默认为`7`。
- `JWT_SECRET`: 用于JWT签名的密钥，默认为`123456`。
- `LOGIN_JS`: 认证失败时重定向的JavaScript文件路径，需从`config.json`中读取。

### Gitee OAuth 集成
- `GITEE_REDIRECT_URI`: Gitee OAuth授权后的回调URL。
- `GITEE_CLIENT_ID`: Gitee应用的客户端ID。
- `GITEE_CLIENT_SECRET`: Gitee应用的客户端密钥。
- `GITEE_OWNER`: Gitee仓库的所有者用户名或组织名。
- `GITEE_REPO`: 需要检查用户是否Star的Gitee仓库名。

## 二、核心功能

### 权限校验中间件
实现了一个自定义的权限校验中间件，用于验证请求路径是否需要权限，以及检查请求携带的令牌是否有效。

### 静态资源服务
使用`express.static`中间件为客户端提供静态资源，资源位于`dist`目录下。

### 登录处理
- 路由: `/doLogin`
- 功能: 根据查询参数`password`验证用户身份，验证成功则设置JWT令牌至Cookie中。

### Gitee OAuth 登录回调
- 路由: `/giteeCallback`
- 流程:
  1. 使用授权码换取Access Token。
  2. 检查用户是否已Star指定的Gitee仓库。
  3. 成功则设置JWT令牌至Cookie，否则重定向至错误页面。

### JWT处理
- 创建: 使用`createToken`函数根据指定过期时间生成JWT。
- 验证: 使用`verifyToken`函数验证JWT的有效性。

## 三、运行与部署
确保所有必要的环境变量已设置，然后通过命令行运行：
```sh
node server.js