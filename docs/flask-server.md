---
needAuth: true
---
# Flask 应用服务器文档

## 一、环境变量配置

### 基础配置
- `JWT_SECRET`: JWT令牌的签名密钥，默认为`123456`。
- `COOKIE_MAX_AGE`: Cookie的最大生命周期（秒），默认为`604800`（7天）。
- `JWT_EXPIRES_IN`: JWT令牌的过期时间（秒），默认为`604800`（7天）。
- `LOGIN_JS`: 认证失败时重定向的JavaScript文件路径，默认为`/login.js`。
- `NEED_AUTH_URLS`: 需要权限验证的URL列表，以JSON数组形式提供，默认为`["/protected"]`。
- `DEFAULT_TOKENS`: 默认允许的认证令牌列表，以逗号分隔，默认值为`123456`。

### Gitee OAuth 集成
- `GITEE_REDIRECT_URI`: Gitee OAuth授权后的回调URL。
- `GITEE_CLIENT_ID`: Gitee应用的客户端ID。
- `GITEE_CLIENT_SECRET`: Gitee应用的客户端密钥。
- `GITEE_OWNER`: Gitee仓库的所有者用户名或组织名。
- `GITEE_REPO`: 需要检查用户是否Star的Gitee仓库名。

## 二、核心功能

### 静态文件服务
- 路由: `/<path:path>`
- 功能: 提供`dist`目录下的静态文件。

### 登录处理
- 路由: `/doLogin`
- 方法: GET
- 功能: 根据查询参数`password`验证用户身份，验证成功则设置JWT令牌至Cookie中。

### 权限校验
- 机制: 使用`@app.before_request`装饰器实现全局请求前的鉴权逻辑。
- 行为: 对于需要验证的URL，检查Cookie中的JWT或默认令牌的有效性。

### JWT处理
- 函数: `create_jwt`
  - 功能: 生成JWT令牌。
- 函数: `verify_jwt`
  - 功能: 验证JWT令牌的有效性。

### Gitee OAuth 登录回调
- 路由: `/giteeCallback`
- 功能:
  1. 使用授权码换取Access Token。
  2. 检查用户是否已Star指定的Gitee仓库。
  3. 成功则设置JWT令牌，否则重定向至错误页面。

## 三、运行与部署
确保所有必要的环境变量已设置，然后通过命令行运行：
```sh
python server.py