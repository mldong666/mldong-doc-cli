import { viteBundler } from '@vuepress/bundler-vite'
import { defaultTheme } from '@vuepress/theme-default'
import { defineUserConfig } from 'vuepress'
import AuthPlugin from './plugins/vuepress-auth-plugin'
export default defineUserConfig({
  plugins: [
    AuthPlugin
  ],
  bundler: viteBundler(),
  lang: 'zh-CN',
  title: 'mldong-doc-cli',
  description: '结合Express的VuePress站点动态权限控制',
  theme: defaultTheme({
      logo: '/logo.png',
      prev: '上一页',
      next: '下一页',
      lastUpdated: false,
      contributors: false,
      navbar: [
        { text: '首页', link: '/' },
        { text: '文档', link: '/quick-start.html' },
        { text: '码云', link: 'https://gitee.com/mldong666/mldong-doc-cli' },
      ],
      sidebar:[
        {
          text: '快速开始',
          collapsible: true,
          link: '/quick-start.md'
        },
        {
          text: '后端服务',
          children: [
            {
              text: 'Express服务',
              link: '/express-server.md'
            },
            {
              text: 'Flask服务',
              link: '/flask-server.md'
            },
          ]
        },
        {
          text: '权限测试',
          collapsible: true,
          link: '/auth-test.md'
        },
        {
          text: '权限测试2',
          collapsible: true,
          link: '/auth-test2.md'
        },
      ]
  }),
})
