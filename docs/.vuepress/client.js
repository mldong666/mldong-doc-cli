import { defineClientConfig } from 'vuepress/client'
import Login from './components/Login.vue'
import Error from './components/Error.vue'

export default defineClientConfig({
  enhance({ app, router, siteData }) {
    app.component('Login',Login)
    app.component('Error',Error)
    app.config.globalProperties.$siteData = siteData
  }
})
